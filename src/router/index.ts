import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ProfitCalculator from "../views/ProfitCalculator.vue";
import MaxAvgDownTradeCalculator from "../views/MaxAvgDownTradeCalculator.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/profit-calculator",
      name: "profitCalculator",
      component: ProfitCalculator
    },
    {
      path: "/max-grid-calculator",
      name: "maxGridCalculator",
      component: MaxAvgDownTradeCalculator
    },
  ],
});

export default router;
