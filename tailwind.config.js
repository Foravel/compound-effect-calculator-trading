/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      boxShadow: {
        'neumorphic': '-.2rem -.2rem .5rem white, .3rem .3rem .6rem #c8d0e7',
        'neumorphic-inset' : 'inset -.2rem -.2rem .5rem white, inset .3rem .3rem .5rem #c8d0e7',
        'neumorphic-dark': '-.3rem -.3rem .6rem #40484e, .3rem .3rem .6rem #24282c',
        'neumorphic-dark-inset': 'inset .1rem .1rem .2rem #202427, inset -.1rem -.1rem .2rem #3f464c',
      },
      borderRadius: {
        'neumorphic': '2.5rem'
      }
    },
  },
  plugins: [],
}
