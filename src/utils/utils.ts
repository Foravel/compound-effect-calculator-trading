export function isNumber(value: number): boolean {
  return typeof value === "number" && isFinite(value);
}

export function isInteger(value: number): boolean {
  return value % 1 === 0;
}
