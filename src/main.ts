import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import "./assets/main.css";

import * as Vue from 'vue' // in Vue 3
import axios from 'axios'
import VueAxios from 'vue-axios'

import VueGtag from "vue-gtag";

const app = createApp(App);

app.use(VueGtag, {
    config: { id: "G-9N0YN3VVFG" }
  });
app.use(createPinia());
app.use(router);
app.use(VueAxios, axios)
app.mount("#app");
