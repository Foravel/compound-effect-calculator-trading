import { test, expect } from "vitest";
import { isNumber, isInteger } from "../../src/utils/utils";

test("isNumber", () => {
    expect(isNumber(1)).toBe(true);
    expect(isNumber(1.1)).toBe(true);
    expect(isNumber(Infinity)).toBe(false);
    expect(isNumber(NaN)).toBe(false);
});

test("isInteger", () => {
    expect(isInteger(1)).toBe(true);
    expect(isInteger(1.1)).toBe(false);
    expect(isInteger(Infinity)).toBe(false);
    expect(isInteger(NaN)).toBe(false);
});

