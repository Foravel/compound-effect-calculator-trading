import { test, expect, describe, it } from "vitest";
import { createPinia } from "pinia";
import { mount } from "@vue/test-utils";
import ExpectedResult from "../../src/components/calculator/result/ExpectedResult.vue";

it("ExpectedResult", () => {
  const pinia = createPinia();
  const wrapper = mount(ExpectedResult, {
    global: { plugins: [pinia] },
  });

  // Arrange
  const initialDeposit = 1000;
  const period = 5;
  const depositForLotSize = 1000;
  const gainPerPeriod = 2;
  const initialLotSize = 0.01;
  const balance = 1000;

  const componentInstance = wrapper.vm;

  // Act
  const { _results, _labels, _datas } = componentInstance.getProfit(
    initialDeposit,
    period,
    depositForLotSize,
    gainPerPeriod,
    initialLotSize,
    balance
  );
  // Assert

  expect(componentInstance).toBeTruthy();
  expect(_results).toEqual([
    { monthNumber: 5, newBalance: 1100, lotSize: 0.01, gain: 20 },
    { monthNumber: 4, newBalance: 1080, lotSize: 0.01, gain: 20 },
    { monthNumber: 3, newBalance: 1060, lotSize: 0.01, gain: 20 },
    { monthNumber: 2, newBalance: 1040, lotSize: 0.01, gain: 20 },
    { monthNumber: 1, newBalance: 1020, lotSize: 0.01, gain: 20 },
  ]);
  expect(_labels).toEqual([1, 2, 3, 4, 5]);
  expect(_datas).toEqual([1020, 1040, 1060, 1080, 1100]);
});
