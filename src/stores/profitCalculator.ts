import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useProfitCalculatorStore = defineStore("profitCalculator", () => {
    const initialDeposit = ref(1000);
    const period = ref(36);
    const gainPerPeriod = ref(2);
    const initialLotSize = ref(0.01);
    const depositForLotSize = ref(1000);

    function updateInitialDeposit(value: number) {
        initialDeposit.value = value;
    }

    function updatePeriod(value: number) {
        period.value = value;
    }

    function updateGainPerPeriod(value: number) {
        gainPerPeriod.value = value;
    }

    function updateInitialLotSize(value: number) {
        initialLotSize.value = value;
    }

    function updatedepositForLotSize(value: number) {
        depositForLotSize.value = value;
    }

    return { initialDeposit, period, gainPerPeriod, initialLotSize, depositForLotSize, updateInitialDeposit, updatePeriod, updateGainPerPeriod, updateInitialLotSize, updatedepositForLotSize };
});

